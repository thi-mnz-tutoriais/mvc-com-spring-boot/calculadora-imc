package com.thimnz.calculadoraimc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.thimnz.calculadoraimc.dto.UserDTO;
import com.thimnz.calculadoraimc.entity.UserEntity;
import com.thimnz.calculadoraimc.enums.ClassificacaoIMC;
import com.thimnz.calculadoraimc.repository.UserRepository;

@Service
public class UserService {
	
	private UserRepository repository;
	
	public UserService(UserRepository repository) {
		this.repository = repository;
	}
	
	public void save(UserDTO user) {
		UserEntity userToSave = new UserEntity();
		userToSave.setId(user.getId());
		userToSave.setName(user.getName());
		userToSave.setHeight(user.getHeight());
		userToSave.setWeight(user.getWeight());
		this.repository.save(userToSave);
	}
	
	public List<UserDTO> findAll() {
		Iterable<UserEntity> userEntityList = this.repository.findAll();
		List<UserDTO> usersList = new ArrayList<>();
		userEntityList.forEach(u -> {
			UserDTO user = new UserDTO();
			user.setId(u.getId());
			user.setName(u.getName());
			user.setImcValue(u.getWeight() / Math.pow(u.getHeight(), 2));
			this.calculateIMC(user);
			usersList.add(user);
		});
		return usersList;
	}
	
	public UserDTO findById(long id) {
		Optional<UserEntity> userEntityOptional = this.repository.findById(id);
		if(userEntityOptional.isPresent()) {
			UserEntity userEntity = userEntityOptional.get();
			UserDTO user = new UserDTO();
			user.setId(userEntity.getId());
			user.setName(userEntity.getName());
			user.setHeight(userEntity.getHeight());
			user.setWeight(userEntity.getWeight());
			this.calculateIMC(user);
			return user;
		} else {
			return null;
		}
	}
	
	public void deleteById(long id) {
		this.repository.deleteById(id);
	}
	
	private void calculateIMC(UserDTO user) {
		for(ClassificacaoIMC imc : ClassificacaoIMC.values()) {
			if(imc.minRange == null) {
				if(user.getImcValue() < imc.maxRange) {
					user.setImc(imc.label);
				}
			} else if(imc.maxRange == null) {
				if(user.getImcValue() >= imc.minRange) {
					user.setImc(imc.label);
				}
			} else {
				if(user.getImcValue() >= imc.minRange && user.getImcValue() < imc.maxRange) {
					user.setImc(imc.label);
				}
			}
		}
	}
}
