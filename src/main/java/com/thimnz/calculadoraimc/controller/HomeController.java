package com.thimnz.calculadoraimc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
	/*
	 * Redirect para lista de usuários
	 * */
	@GetMapping
	public ModelAndView main() {
		return new ModelAndView("redirect:/usuarios");
	}
}
