package com.thimnz.calculadoraimc.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thimnz.calculadoraimc.dto.UserDTO;
import com.thimnz.calculadoraimc.service.UserService;

@Controller
@RequestMapping("/usuarios")
public class UserController {
	
	private UserService service;
	
	public UserController(UserService service) {
		this.service = service;
	}
	
	private UserDTO getUserDTO() {
		return new UserDTO();
	}
	
	@GetMapping
	public ModelAndView main() {
		ModelAndView view = new ModelAndView("users-list");
		view.addObject("users", this.service.findAll());
		return view;
	}
	
	@GetMapping("/novo")
	public ModelAndView novoUsuario() {
		ModelAndView view = new ModelAndView("users-form");
		view.addObject("userDTO", this.getUserDTO());
		return view;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editarUsuario(@PathVariable("id") final long id) {
		ModelAndView view = new ModelAndView("users-form");
		view.addObject("userDTO", this.service.findById(id));
		return view;
	}
	
	@PostMapping("/salvar")
	public ModelAndView salvarUsuarios(@Valid UserDTO userDTO, BindingResult result, RedirectAttributes redirectAttributes) {
		
		if(result.hasErrors()) {
			return new ModelAndView("users-form");
		}
		
		this.service.save(userDTO);
		redirectAttributes.addFlashAttribute("sucesso", "Usuário " + userDTO.getName() + " salvo com sucesso. ");
		return new ModelAndView("redirect:/usuarios");
	}
	
	@GetMapping("/excluir/{id}")
	public ModelAndView excluirUsuario(@PathVariable("id") final long id, RedirectAttributes redirectAttributes) {
		UserDTO userToDelete = this.service.findById(id);
		if(userToDelete == null) {
			redirectAttributes.addFlashAttribute("erro", "Não foi possível excluir, pois este usuário não existe. ");
		} else {
			this.service.deleteById(id);
			redirectAttributes.addFlashAttribute("sucesso", "Usuário " + userToDelete.getName() + " excluído com sucesso. ");
		}
		return new ModelAndView("redirect:/usuarios");
	}
}
