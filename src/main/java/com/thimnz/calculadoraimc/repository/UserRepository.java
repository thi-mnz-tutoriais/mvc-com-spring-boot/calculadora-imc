package com.thimnz.calculadoraimc.repository;

import org.springframework.data.repository.CrudRepository;

import com.thimnz.calculadoraimc.entity.UserEntity;

public interface UserRepository extends CrudRepository <UserEntity, Long>{

}
