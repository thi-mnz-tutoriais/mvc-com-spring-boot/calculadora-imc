package com.thimnz.calculadoraimc.enums;

public enum ClassificacaoIMC {
	
	ABAIXO(null, 18.5, "Abaixo do peso"),
	NORMAL(18.5, 25.0, "Peso normal"),
	SOBREPESO(25.0, 30.0, "Sobrepeso"),
	OBESIDADE_GRAU_1(30.0, 35.0, "Obesidade grau I"),
	OBESIDADE_GRAU_2(35.0, 40.0, "Obesidade grau II"),
	OBESIDADE_GRAU_3(40.0, null, "Obesidade grau III");
	
	public final Double minRange;
	public final Double maxRange;
	public final String label;
	
	private ClassificacaoIMC(Double min, Double max, String label) {
		this.minRange = min;
		this.maxRange = max;
		this.label = label;
	}
}
