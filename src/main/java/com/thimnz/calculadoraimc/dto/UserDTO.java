package com.thimnz.calculadoraimc.dto;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

public class UserDTO {
	
	private long id;
	
	@NotBlank(message = "Por favor, forneça um nome. ")
	private String name;
	
	@DecimalMin(value = "0.50", message="Por favor, forneça uma altura maior ou igual a 0,5m (50 cm)")
	@DecimalMax(value = "2.60", message="Por favor, forneça uma altura menor ou igual a 2,6m")
	private double height;
	
	@DecimalMin(value = "0.50", message="Por favor, forneça uma peso maior ou igual a 0,5kg (500g)")
	@DecimalMax(value = "500.00", message="Por favor, forneça uma peso menor ou igual a 500kg")
	private double weight;
	
	private double imcValue;
	
	private String imc;
	
	public UserDTO() {
		
	}
	
	public UserDTO(String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}
	
	public UserDTO(long id, double imc, String classificacao) {
		this.id = id;
		this.imcValue = imc;
		this.imc = classificacao;
	}
	
	public UserDTO(long id, String name, double height, double weight) {
		this.id = id;
		this.name = name;
		this.height = height;
		this.weight = weight;
	}
	
	public UserDTO(long id, String name, double height, double weight, double imc, String classificacao) {
		this.id = id;
		this.name = name;
		this.height = height;
		this.weight = weight;
		this.imcValue = imc;
		this.imc = classificacao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getImcValue() {
		return imcValue;
	}

	public void setImcValue(double imcValue) {
		this.imcValue = imcValue;
	}

	public String getImc() {
		return imc;
	}

	public void setImc(String imc) {
		this.imc = imc;
	}
}
