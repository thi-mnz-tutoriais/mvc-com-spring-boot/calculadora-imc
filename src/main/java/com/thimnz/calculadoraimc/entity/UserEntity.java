package com.thimnz.calculadoraimc.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.thimnz.calculadoraimc.enums.ClassificacaoIMC;

@Entity
public class UserEntity implements Serializable {
	private static final long serialVersionUID = -7429253088895962768L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private double height;
	private double weight;
	
	private transient ClassificacaoIMC imc;
	
	public UserEntity(long id, String name, double height, double weight) {
		this.id = id;
		this.name = name;
		this.height = height;
		this.weight = weight;
	}

	public UserEntity() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public ClassificacaoIMC getImc() {
		return imc;
	}

	public void setImc(ClassificacaoIMC imc) {
		this.imc = imc;
	}
}
