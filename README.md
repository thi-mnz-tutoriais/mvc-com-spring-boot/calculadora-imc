# calculadora-imc

Cadastro de usuários e cálculo de seus IMCs.

Este projeto visa demonstrar a utilização de recursos mais complexos empregados no Spring, como JPA/Hibernate e Spring Data, além de empregar a utilização dos design patterns Service, Repository e DAO.